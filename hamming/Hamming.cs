using System;

public static class Hamming
{
    public static int Distance(string firstStrand, string secondStrand)
    {
        int length = firstStrand.Length != secondStrand.Length 
            ? throw new ArgumentException() 
            : firstStrand.Length;
        Console.WriteLine($" checking the dinstance of {firstStrand} and {secondStrand}");
        int distance = 0;
        char[] firstStrandArray = firstStrand.ToCharArray();
        char[] secondStrandArray = secondStrand.ToCharArray();
        for (int i = 0; i < length; i++)
        {
            distance += firstStrandArray[i] == secondStrandArray[i] ? 0 : 1; 
        }
        Console.WriteLine($" Result: instance {distance}");
        return distance;
    }
}